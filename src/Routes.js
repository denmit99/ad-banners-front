import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import CategoryComponent from './component/CategoryComponent';
import Home from './component/Home'

export default function Routes() {
    return (
        <BrowserRouter>
            <Home>
                
                <Route path="/categories" component={CategoryComponent} />
            </Home>
        </BrowserRouter>
    );
}
