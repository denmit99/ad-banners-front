import CategoryService from '../service/CategoryService'
import React, { useState, useEffect, useCallback } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import TextField from '@material-ui/core/TextField';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';

export default function CategoryComponent(props) {

    const [categories, setCategories] = useState([])
    const [searchText, setSearchText] = useState("")
    const [id, setId] = useState(-1)
    const [name, setName] = useState("name")
    const [reqName, setReqName] = useState("reqName")

    const update = useCallback(() => {
        return CategoryService.getCategories(searchText).then((response) => {
            setCategories(response.data)
        });
    }, [searchText]);

    const handleSearchTextChange = (e) => {
        setSearchText(e.target.value)
    }

    const handleNameChange = (e) => {
        setName(e.target.value)
    }

    const handleReqNameChange = (e) => {
        setReqName(e.target.value)
    }

    const handleSave = (event) => {
        if(id===-1){
            CategoryService.addCategory({ name: name, reqName: reqName })
        } else {
            CategoryService.editCategory({id: id, name: name, reqName: reqName })
        }
    }

    const handleDelete = () => {
        CategoryService.deleteCategory(id)
    }

    const listElement = (el) => {
        return (
            <ListItem
                button key={el.id}
                onClick={() => { categoryClickHandler(el) }}
            >
                <ListItemText primary={el.name} />
            </ListItem>
        )
    }

    useEffect(() => {
        update()
    }, [searchText])

    const categoryClickHandler = (category) => {
        setId(category.id)
        setName(category.name)
        setReqName(category.reqName)
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={3}>
            <div>
            <Paper component="form" >
                <InputBase
                    placeholder="Search categories"
                    onChange={handleSearchTextChange}
                />
                <IconButton type="submit" aria-label="search">
                    <SearchIcon />
                </IconButton>
            </Paper>
            <div>
                <List component="nav">
                    {categories.map(category => listElement(category))}
                </List>
            </div>
            <Button
                variant="contained"
                color="primary"
                startIcon={<AddIcon />}
            >
                Create new category
            </Button>
        </div>
               
            </Grid>
            <Grid item xs={9}>
                

            <div>
                <Grid container
                    direction="column"
                    justify="flex-start"
                    alignItems="center"
                >
                    <Grid item> <TextField id="standard-basic" label="Name" onChange={handleNameChange} value={name} /></Grid>
                    <Grid item><TextField id="standard-basic" label="Request ID" onChange={handleReqNameChange} value={reqName} /></Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="default"
                            startIcon={<SaveIcon />}
                            onClick={handleSave}
                        >
                            Save
                                </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<DeleteIcon />}
                            onClick={handleDelete}
                        >
                            Delete
                                </Button>
                    </Grid>
                </Grid>
            </div>


            </Grid>
        </Grid>
    );
}
