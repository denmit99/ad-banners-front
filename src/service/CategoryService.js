import axios from 'axios'
import API from '../utils/API'
import getBaseURL from '../utils/API'

const CATEGORY_URL = getBaseURL() + "/category"

class CategoryService {
    getCategories(name) {
        return axios.get(CATEGORY_URL + "/search?name=" + name);
    }

    getCategoryById() {
        return axios.get(CATEGORY_URL + "/id");
    }

    addCategory(category){
        return axios.post(CATEGORY_URL, category)
    }

    editCategory(category){

        return axios.put(CATEGORY_URL + "/edit", category)
    }

    deleteCategory(id){
        return axios.put(CATEGORY_URL + "/delete/" + id).then((response) => alert(response.status))
    }
}

export default new CategoryService();