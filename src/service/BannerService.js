import axios from 'axios'
import getBaseURL from '../utils/API'

const BANNER_URL = getBaseURL() + "/banner"

class BannerService {
    getBanners() {
        axios.get(BANNER_URL);
    }

    getBannerById()
}